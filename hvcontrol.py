#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os
import serial
import serial.tools.list_ports
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QSpinBox
from PyQt5 import QtCore, QtGui
import logging as myLogger

myLogger.basicConfig(format='%(asctime)s %(levelname)s:%(message)s',
                   level=myLogger.INFO)

CAEN_VID  = 0x21e1
CAEN_BAUD = 9600

V_DEFAULT = [200,150,100]#[2100, 1470, 1000] # DBT for GEM test


class gMainWindow(QWidget):
    """ Simple gui for HV control.
    Just buttons (Start/Stop, Quit).
    Readout sync with QTimer timeout with selectable time interval
    TODO: 
    1) Save and Plot  values
    """

    def __init__(self):
        super(gMainWindow, self).__init__()
        self.setupUI()
        self.isON = False
        serId = self.__scan_serial()
        self.ser = None
        if serId != None:
            myLogger.info("Found CAEN board at %s" % serId)
            self.connect(serId)
        else:
            myLogger.error("CAEN board NOT found!!!")
        
        (self.Vd_set, self.Vt_set, self.Vb_set) = self.readSetValues()
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.readValues)
        self.timer.start(1000)

        
    def setupUI(self):
        self.setGeometry(100, 100, 400, 300)
        self.setWindowTitle('CAEN HV control')

        # buttons setup
        self.btnOnOff = QPushButton('On/Off',self)
        self.btnOnOff.setFixedSize(70, 60)
        self.btnOnOff.move(220, 20)
        self.btnOnOff.clicked.connect(self.ToggleOnOff)
        self.btnOnOff.setFont(QtGui.QFont('SansSerif', 12))

        self.btnUpdate = QPushButton('Update', self)
        self.btnUpdate.move(220, 100)
        self.btnUpdate.setFont(QtGui.QFont('SansSerif', 12))
        self.btnUpdate.clicked.connect(self.updateValues)
        
        btnQuit = QPushButton('Quit',self)
        btnQuit.move(220, 220)
        btnQuit.clicked.connect(self.CloseAll)

        # add status
        txtStatus0 = QLabel("HV status:", self)
        txtStatus0.move(300, 20)
        self.txtStatus = QLabel("", self)
        self.txtStatus.move(320, 50)
        self.txtStatus.setText("<font color='green'>OFF</font>")


        # add labels
        txtSet = QLabel("V Settings", self)
        txtSet.move(70, 0)
        self.lineDrift = QSpinBox(self)
        self.lineDrift.setRange(0, 3000)
        self.lineDrift.setFixedSize(80, 30)
        self.lineDrift.move(100, 30)
        txtDrift = QLabel("V Drift:", self)
        txtDrift.move(50, 30)

        self.lineTop = QSpinBox(self)
        self.lineTop.setRange(0, 2000)
        self.lineTop.setFixedSize(80, 30)
        self.lineTop.move(100, 70)
        txtTop = QLabel("V Top:", self)
        txtTop.move(50, 70)

        self.lineBot = QSpinBox(self)
        self.lineBot.setRange(0, 2000)
        self.lineBot.setFixedSize(80, 30)
        self.lineBot.move(100, 110)
        txtBot = QLabel("V Bot:", self)
        txtBot.move(50, 110)

        # set default V value
        self.lineDrift.setValue(V_DEFAULT[0])
        self.lineTop.setValue(V_DEFAULT[1])
        self.lineBot.setValue(V_DEFAULT[2])

        # add label monitor values
        txtMon = QLabel("V Monitor", self)
        txtMon.move(70, 145)
        self.monDrift = QLabel("mon Vd", self)
        self.monDrift.setFixedSize(80, 30)
        self.monDrift.move(100, 170)
        txtDriftm = QLabel("V Drift:", self)
        txtDriftm.move(50, 170)

        self.monTop = QLabel("mon Vt", self)
        self.monTop.setFixedSize(80, 30)
        self.monTop.move(100, 200)
        txtTopm = QLabel("V Top:", self)
        txtTopm.move(50, 200)

        self.monBot = QLabel("mon Vb", self)
        self.monBot.setFixedSize(80, 30)
        self.monBot.move(100, 230)
        txtBotm = QLabel("V Bot:", self)
        txtBotm.move(50, 230)
    
    def ToggleOnOff(self):
        if self.isON:
            # turn OFF
            self.turnOff()
            self.isON = False
            self.txtStatus.setText("<font color='green'>OFF</font>")
        else:
            # turn ON
            self.updateValues()
            self.turnOn()
            self.isON = True
            self.txtStatus.setText("<font  color='red'>ON</font>")

    def CloseAll(self):
        myLogger.info("bye!")
        QtCore.QCoreApplication.instance().quit()

    def __scan_serial(self):
        """ scan usb port and return the address of the first device with right vid
        """
        port = serial.tools.list_ports.comports()
        for p in port:
            #print("%s - %s - %s - vid:%s - pid:%s - %s " % \
                #      (p.device, p.name, p.description, p.vid, p.pid, p.hwid))
            if p.vid == CAEN_VID:
                return p.device
        return None

    def connect(self, dev):
        self.ser = serial.Serial(dev)
        self.ser.baudrate = 9600
        if not self.ser.isOpen():
            myLogger.info("Cannot connect to %s. Exit" % dev)
            self.CloseAll()
        return 0
            
    def updateValues(self):
        Vd = self.lineDrift.value()
        Vt = self.lineTop.value()
        Vb = self.lineBot.value()
        (self.Vd_set, self.Vt_set, self.Vb_set) = self.readSetValues()
        if self.checkVoltageRules(Vd, Vt, Vb):
            if Vd != self.Vd_set:
                myLogger.info("Set Vd to %d", Vd)
                self.ser.write(b"$BD:00,CMD:SET,CH:1,PAR:VSET,VAL:%.f\r" % Vd)
                msg = self.ser.readline().decode("utf-8")
                print ("%s - %s %s" % (msg.strip('\n'), "Success:", self.checkCmdOut(msg)))
            if Vt != self.Vt_set:
                myLogger.info("Set Vt to %d", Vt)
                self.ser.write(b"$BD:00,CMD:SET,CH:3,PAR:VSET,VAL:%.f\r" % Vt)
                msg = self.ser.readline().decode("utf-8")
                print ("%s - %s %s" % (msg.strip('\n'), "Success:", self.checkCmdOut(msg)))
            if Vb != self.Vb_set:
                myLogger.info("Set Vb to %d", Vb)
                self.ser.write(b"$BD:00,CMD:SET,CH:2,PAR:VSET,VAL:%.f\r" % Vb)
                msg = self.ser.readline().decode("utf-8")
                print ("%s - %s %s" % (msg.strip('\n'), "Success:", self.checkCmdOut(msg)))
        else:
            myLogger.error("Voltage rule error: DTB = %d %d %d" % (Vd, Vt, Vb))

                           
    def turnOn(self):
        myLogger.info("Turning HV ON")
        (self.Vd_set, self.Vt_set, self.Vb_set) = self.readSetValues()
        if self.checkVoltageRules(self.Vd_set, self.Vt_set, self.Vb_set):
            self.ser.write(b"$BD:00,CMD:SET,CH:4,PAR:ON\r")
            msg = self.ser.readline().decode("utf-8")
            print ("%s - %s %s" % (msg.strip('\n'), "Success:", self.checkCmdOut(msg)))
        else:
            myLogger.error("Voltage rule error: DTB = %d %d %d" % (Vd, Vt, Vb))

    def turnOff(self):
        myLogger.info("Turning HV OFF")
        (self.Vd_set, self.Vt_set, self.Vb_set) = self.readSetValues()
        if self.checkVoltageRules(self.Vd_set, self.Vt_set, self.Vb_set):
            self.ser.write(b"$BD:00,CMD:SET,CH:4,PAR:OFF\r")
            msg = self.ser.readline().decode("utf-8")
            print ("%s - %s %s" % (msg.strip('\n'), "Success:", self.checkCmdOut(msg)))
        else:
            myLogger.error("Voltage rule error: DTB = %d %d %d" % (Vd, Vt, Vb))


    def checkCmdOut (self, msg):            
        if "CMD:OK" in msg:
            return True
        else:
            myLogger.error("mgs: %s" % msg)
            return False

    def readSetValues(self):
        self.ser.write(b'$BD:00,CMD:MON,CH:4,PAR:VSET\r')
        msg = self.ser.readline().decode("utf-8")
        vv = msg.strip('\n').split("VAL:")[-1].split(';')
        vd = float(vv[1])
        vb = float(vv[2])
        vt = float(vv[3])
        return (vd, vt, vb)

    def readValues(self):
        self.ser.write(b"$BD:00,CMD:MON,CH:4,PAR:VMON\r")
        msg = self.ser.readline().decode("utf-8")
        vv = msg.strip('\n').split("VAL:")[-1].split(';')
        vd = float(vv[1])
        vb = float(vv[2])
        vt = float(vv[3])
        self.monDrift.setText("%.1f V" % vd)
        self.monTop.setText("%.1f V" % vt)
        self.monBot.setText("%.1f V" % vb)
        return 0
    
    def checkVoltageRules(self, Vd, Vt, Vb):
        if Vt>=Vb and (Vt - Vb)< 600:
            return True
        return False
    
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    vm  = gMainWindow()
    vm.show()
    sys.exit(app.exec_())
